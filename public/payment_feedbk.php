<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/22/2017
 * Time: 9:46 PM
 */

require_once(__DIR__.'/../resources/config.php');

require_once("../resources/boot.php");

?>


<?php include(TEMPLATE_FRONT . DS . "header.php") ?>

<style>

    label.heading {
        font-weight: 600;
    }

    .payment-form {
        width: 300px;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        border: 1px #333 solid;

    }

</style>

<nav class="navbar navbar-inverse">

    <?php include(TEMPLATE_FRONT . DS . "navbar.php") ?>

</nav>


<?php get_payment_info(); ?>



<div class="container" style="text-align: center; margin-top: 100px;">


    <h2>Dear customer, thank you for paying with our Braintree service.</h2><br />
    <form class="payment-form">

        <div class="form-group">
            <label for="ID" class="heading">Transaction ID:</label><br>
            <input type="text" disabled="disabled" class="form-control" id="ID" name="ID" value="<?php echo $result->transaction->id ; ?>" >
        </div>

        <div class="form-group">
            <label for="firstName" class="heading">First Name:</label><br>
            <input type="text" disabled="disabled" class="form-control" id="firstName" name="firstName" value="<?php echo $result->transaction->customer['firstName'] ; ?>" >
        </div>

        <div class="form-group">
            <label for="lastName" class="heading">Last Name:</label><br>
            <input type="text" disabled="disabled" class="form-control" id="lastName" name="lastName" value="<?php echo $result->transaction->customer['lastName'] ; ?>" >
        </div>

        <div class="form-group">
            <label for="amount" class="heading">Amount (USD):</label><br>
            <input type="text" disabled="disabled" class="form-control" id="amount" name="amount" value="<?php echo $result->transaction->amount . " " . $result->transaction->currencyIsoCode ; ?>" >
        </div><br>

        <div class="form-group">
            <label for="status" class="heading">Status:</label><br>
            <input type="text" disabled="disabled" class="form-control" id="status" name="status" value="Successful" >
        </div>


        <div id="dropin-container"></div>
<!--        <button id="submit-button" type="submit">Pay with BrainTree</button>-->

    </form>



</div>

<br><br>

<?php include(TEMPLATE_FRONT . DS . "footer.php") ?>