<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/22/2017
 * Time: 5:37 AM
 */

require_once(__DIR__.'/../resources/config.php');

//require_once(__DIR__.'/../resources/token.php');

?>




<?php include(TEMPLATE_FRONT . DS . "header.php") ?>

<style>

    label.heading {
        font-weight: 600;
    }

    .payment-form {
        width: 300px;
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        border: 1px #333 solid;

    }

</style>

<nav class="navbar navbar-inverse">

    <?php include(TEMPLATE_FRONT . DS . "navbar.php") ?>

</nav>

<div class="container" style="text-align: center; margin-top: 100px;">


    <h2>Pay with Braintree</h2><br />
    <form action="" method="post" class="payment-form">



        <div class="form-group">
            <label for="firstName" class="heading">First Name:</label><br>
            <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Please enter your first name" required>
        </div>

        <div class="form-group">
            <label for="lastName" class="heading">Last Name:</label><br>
            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Please enter your last name" required>
        </div>

        <div class="form-group">
            <label for="amount" class="heading">Amount (USD):</label><br>
            <input type="text" class="form-control" id="amount" name="amount" placeholder="Please enter amount" required>
        </div><br>

        <div id="dropin-container"></div>
        <button id="submit-button" type="submit">Pay with BrainTree</button>

    </form>



</div>

    <br><br>


<?php include(TEMPLATE_FRONT . DS . "footer.php") ?>
