<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/21/2017
 * Time: 1:40 AM
 */

require_once(__DIR__.'/../resources/config.php');

$results = cart();

$products = $results['products'];

?>



<?php include(TEMPLATE_FRONT . DS . "header.php") ?>


<nav class="navbar navbar-inverse">

    <?php include(TEMPLATE_FRONT . DS . "navbar.php") ?>

</nav>

<div class="container">


    <!-- /.row -->

    <div class="row">

        <h1>Checkout</h1>

        <form action="">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Image</th>
                    <th>Remove</th>

                </tr>
                </thead>
                <tbody>
                <?php if(count($products) > 0) { ?>
                    <?php foreach($products as $product) { ?>
                        <tr>
                            <td><?=$product['product_name']?></td>
                            <td><?=$product['product_price']?></td>
                            <td><img style="max-width: 200px" src="../../coffee89/public/admin/uploads/<?=$product['image']?>" class="img-thumbnail"/></td>
                            <td><a href="./cart.php?_id=<?=$product['product_id']?>&action=remove">Remove</a></td>
                        </tr>

                    <?php } ?>

                <?php } else {?>
                    <tr>
                        <td align='center' colspan=4>No items</td>
                    </tr>

                <?php } ?>

                </tbody>
            </table>
        </form>



        <!--  ***********CART TOTALS*************-->

        <div class="col-xs-4 pull-right ">
            <h2>Cart Totals</h2>

            <table class="table table-bordered" cellspacing="0">
                <tbody>

                <tr class="cart-subtotal">
                    <th>Items:</th>
                    <td><span class="amount"><?=$results['number_product']?></span></td>
                </tr>
                <tr class="shipping">
                    <th>Shipping and Handling</th>
                    <td>Free Shipping</td>
                </tr>

                <tr class="order-total">
                    <th>Order Total</th>
                    <td><strong><span class="amount">$<?=$results['total_cost']?></span></strong> </td>
                </tr>



                </tbody>


            </table>

            <table class="table table-bordered" cellspacing="0">

                <p>
<!--                    <a href="https://www.braintreegateway.com/merchants/YOUR_MERCHANT_ID/verified" target="_blank">-->
<!--                    <img src="https://s3.amazonaws.com/braintree-badges/braintree-badge-dark.png" width="164px" height ="44px" border="0"/>-->
<!--                    </a>-->
                    <a class="btn btn-primary btn-lg" href="pay.php" role="button">Pay As You Go</a></p>


            </table>

        </div><!-- CART TOTALS-->


    </div><!--Main Content-->


</div>

</div>    <br><br>


<?php include(TEMPLATE_FRONT . DS . "footer.php") ?>
