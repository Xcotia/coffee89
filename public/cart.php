<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/20/2017
 * Time: 11:14 PM
 */



require_once(__DIR__.'/../resources/config.php');

?>

<?php


    // we first dictate what's coming in, and the first thing coming in is a GET request.
    if (isset($_GET['add']))
    {
        $product_id = intVal($_GET['add']);

        if (!isset($_SESSION['cart_product_id']))
        {
            $_SESSION['cart_product_id'] = [];
        }

        if (!in_array($product_id, $_SESSION['cart_product_id']))
        {
            $_SESSION['cart_product_id'][] = $product_id;
        }

        redirect('checkout.php');
    }


    // this code remove product from the cart
    if (isset($_GET['_id']) && isset($_GET['action']) && $_GET['action'] == 'remove')
    {
        $product_id = intVal($_GET['_id']);

        if (isset($_SESSION['cart_product_id']))
        {
            if (in_array($product_id, $_SESSION['cart_product_id']))
            {
                $key = array_search( $product_id, $_SESSION['cart_product_id']);

                unset($_SESSION['cart_product_id'][$key]);
            }
        }

        redirect('checkout.php');
    }
?>

