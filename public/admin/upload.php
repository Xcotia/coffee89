<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/18/2017
 * Time: 7:15 PM
 */



require_once(__DIR__.'/../../resources/config.php');




?>




<?php include(TEMPLATE_BACK . DS . "header.php") ?>



<!-- Sidebar/menu -->
<?php include(TEMPLATE_BACK . DS . "sidebar.php") ?>



<div class="container">

    <h2>Create Product</h2><br />
    <form action="upload.php" method="post" enctype="multipart/form-data">

        <?php upload_product(); ?>

        <div class="form-group">
            <label for="product_id">Product ID:</label>
            <input type="number" class="form-control" id="product_id" name="product_id" placeholder="Please enter product id accordingly" required>
        </div>

        <div class="form-group">
            <label for="product_name">Product Name:</label>
            <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Please enter the product's name" required>
        </div>
        <div class="form-group">
            <label for="product_sht_desc">Product Short Description:</label>
            <input type="text" class="form-control" id="product_sht_desc" name="product_sht_desc" placeholder="Please enter a short description about the product" required>
        </div>
        <div class="form-group">
            <label for="product_desc">Product Description:</label>
            <textarea class="form-control" rows="5" id="product_desc" name="product_desc" required></textarea>
        </div>
        <div class="form-group">
        <label for="categories">Select Product Category</label>
        <select name="product_category" class="form-control">
            <?php select_category(); ?>
        </select>
        </div>

        <div class="form-group">
            <label for="product_qty">Product Quantity:</label>
            <input type="number" class="form-control" id="product_qty" name="product_qty" placeholder="Please enter quantity of products available" required>
        </div>
        <div class="form-group">
            <label for="product_price">Product Price:</label>
            <input type="number" class="form-control" id="product_price" name="product_price" placeholder="Please enter price per product" required>
        </div>
        <div class="form-group">
            <label for="image">Product Image:</label>
            <input type="file" name="image" id="image" required/>
        </div><br />




        <button type="submit" name="upload" class="btn btn-primary">Create Product</button>
    </form>

</div>

<?php include(TEMPLATE_BACK . DS . "footer.php") ?>
