<?php

require_once('../../resources/DB.php');

require_once("../../resources/config.php");

$db = new DB();
?>

<?php include(TEMPLATE_BACK . DS . "header.php") ?>



<!-- Sidebar/menu -->
<?php include(TEMPLATE_BACK . DS . "sidebar.php") ?>

<?php

// check if we have data to insert information about category to the mongodb database.
if (isset( $_POST['cat_title'])) {
    if (! empty($_POST['cat_title'])) {
        $insertable = $db->insertNewCat([
            'cat_title' => $_POST['cat_title']
        ]);

        if ( $insertable )
        {
            ?>
    <div class="container">
        <div class="panel">
            <h3>New category has been inserted.</h3>
        </div>
    </div>
    <?php
        }
    }
}
?>

<div class="container">
    <h2>Create Category</h2>
    <form action="create_cat.php" method="POST">
        <div class="form-group">
            <label for="category">Category Name:</label>
            <input class="form-control" id="focusedInput" name="cat_title" type="text" value="Please enter product category name" />
        </div>
        <button type="submit" class="btn btn-primary">Enter Category</button>
    </form>
</div>

<!--<br><br><br><br><br><br><br>-->


<?php include(TEMPLATE_BACK . DS . "footer.php") ?>