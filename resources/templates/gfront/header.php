<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/20/2017
 * Time: 8:42 PM
 */


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Coffee89</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://js.braintreegateway.com/js/braintree-2.31.0.min.js"></script>

    <!-- setting up braintree -->
    <script>

        $.ajax({
            url:    "../resources/token.php",
            type:   "get",
            dataType:   "json",
            success:    function (data)
            {
                braintree.setup(data,'dropin', {
                        container:  'dropin-container'
                    }
                );
            }
        })


    </script>



    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
    margin-bottom: 50px;
            border-radius: 0;
        }

        /* Remove the jumbotron's default bottom margin */
        .jumbotron {
    margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
    background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

<div class="jumbotron">
    <div class="container text-center">
        <h1>Coffee89</h1>
        <p><strong>Our Mission: </strong>We make you love to drink coffee.</p>
    </div>
</div>
