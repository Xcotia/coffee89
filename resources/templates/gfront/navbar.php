<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/20/2017
 * Time: 8:47 PM
 */


?>

<div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Coffee89</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>

            <li role="presentation" class="dropdown">
               <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Categories <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#">HTML</a></li>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">About Us</a></li>

                </ul>

            </li>


<!--            <li><a href="#">Product Categories</a></li>-->
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="checkout.php"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
<!--            <li><a href="#"><span class="glyphicon glyphicon-user"></span> Your Account</a></li>-->

        </ul>
    </div>
</div>

