<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/20/2017
 * Time: 5:05 AM
 */

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin Dashboard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {height: 550px}

        /* container fluid */
        .container-fluid {
            background-color: sienna;
        }

        /* Set gray background color and 100% height */
        .sidenav {
            background-color: sienna;
            height: 60em;

            width: 18%;

        }

        /* On small screens, set height to 'auto' for the grid */
        @media screen and (max-width: 767px) {
            .row.content {height: auto;}
        }

        @media (min-width: 1000px)
        grid.less:7
            .container {
                width: 700px;
            }

            .form-control {
                display: block;
                width: 75%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                -webkit-box-shadow:
            }

            body {
                 margin: 0;
            }



                /* Style the footer */
        .footer {
            background-color: sienna;
            padding: 10px;
            text-align: center;
        }

    </style>
</head>
<body>

<!--<nav class="navbar navbar-inverse visible-xs">-->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://localhost/gre.uk/coffee89/public/index.php">Coffee89</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Admin Dashboard</a></li>
                <li><a href="upload.php">Create Product</a></li>
                <li><a href="#">Show Products</a></li>
                <li><a href="create_cat.php">Create Category</a></li>
                <li><a href="#">Show Categories</a></li>
            </ul>
        </div>
    </div>
</nav>
