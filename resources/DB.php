<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/19/2017
 * Time: 12:45 AM
 */



require_once(__DIR__.'/../vendor/autoload.php');




class DB
{


    function __construct()
    {
        $this->db = ( new MongoDB\Client)->coffeedb;


        $this->category = $this->db->category;
        $this->products = $this->db->products;

    }

    public function findByProductId($id){
        return $this->products->findOne(['product_id' => (string)$id]);
    }
    // function to create category
    public function insertNewCat( $categoryInfo = [] )
    {
        if ( empty ( $categoryInfo ) )
        {
            return false;
        }

        // lets insert category
        $insertable = $this->category->insertOne([
            'cat_title' => $categoryInfo['cat_title']
        ]);

        // return this inserted documents mongodb id.
        return $insertable->getInsertedId();

    }


    }