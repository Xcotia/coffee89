<?php
/**
 * Created by PhpStorm.
 * User: nightBlue
 * Date: 11/21/2017
 * Time: 1:25 AM
 */


?>

<?php


// this function just redirects any time we needed to.
function redirect($location)
{

    return header("Location: $location");

}


// this function processes uploading of product for the admin
function upload_product($productInfo = [])
{


    //  image types that can be uploaded
    $allowed_types = array('image/png', 'image/jpg', 'image/jpeg');

    // if the upload button is pressed
    if ($_POST) {
        // the path to store the uploaded image
        $image_name = $_FILES['image']['name'];

        // image types must be according to the specified
        $image_type = $_FILES['image']['type'];


        // location of the image
        $tmp_name = $_FILES['image']['tmp_name'];

        // connecting to the database
        $connection = new DB();

        // linking to the collection
        $collection = $connection->products;


        // receiving POST request from the form.
        $productInfo['image'] = $_FILES['image']['name'];
        $productInfo['product_id'] = $_POST['product_id'];
        $productInfo['product_name'] = $_POST['product_name'];
        $productInfo['product_sht_desc'] = $_POST['product_sht_desc'];
        $productInfo['product_desc'] = $_POST['product_desc'];
        $productInfo['product_category'] = $_POST['product_category'];
        $productInfo['product_qty'] = $_POST['product_qty'];
        $productInfo['product_price'] = $_POST['product_price'];

        // saving the data to collection
        $collection->insertOne($productInfo);

        // ensuring image validation
        if (in_array($image_type, $allowed_types)) {
            // Now we move the uploaded image into the folder images
            move_uploaded_file($tmp_name, "uploads/" . $image_name);

            echo "Ok, to go..";
        } else {
            echo "Please only images are allowed";
        }

    }

}


/** Displaying our available products to our consumers
 */
function available_products()
{

    // connect to our database
    $connection = new DB;

    // linking the desired collection we want to use
    $collection = $connection->products;

    // querying our collection, so as to retrieve products
    $query = $collection->find();

    // fetching the data and using it, just the way we want it..

    $products = $query;

    foreach ($products as $product) {

        $get = <<<DELIMETER

    <div class="col-sm-4 col-md-4">
        <div class="thumbnail">
            <img src="admin/uploads/{$product['image']}"  class="img-responsive" style="width:100% height:100%" alt="Image" />
            <div class="caption">
                <h3>{$product['product_name']}</h3>
                <p>{$product['product_sht_desc']}</p>
                <p><a href="cart.php?add={$product['product_id']}" class="btn btn-primary" role="button">ADD TO CART</a> <a class="pull-right btn btn-default" role="button">&#36;{$product['product_price']}</a></p>
            </div>
        </div>
    </div>



DELIMETER;

        echo $get;

    }


}


/** this function queries the category collection and displays
 *   available categories on the product upload page.
 **/
function select_category()
{
    $db = new DB();
    $mObj = $db->category->find();

    foreach ($mObj as $row) {
        $category_links = <<<DELIMETER

<option value = "{ $row->_id }"> $row->cat_title </option>
DELIMETER;
        echo $category_links;

    }
}


/** this function displays the added products in cart
 **/
function cart()
{
    $results = [
        'total_cost' => 0,
        'number_product' => 0,
        'products' => []
    ];

    if (isset($_SESSION['cart_product_id'])) {
        $products = $_SESSION['cart_product_id'];
        $results['number_product'] = count($products);
        // connection to the database
        $db = new DB();

        // linking to the collection
        $collection = $db->products;

        foreach ($products as $product_id) {
            // retrieving data from the collection
            $pr = $db->findByProductId($product_id);
            if ($pr) {
                $results['products'][] = $pr;
                $results['total_cost'] += $pr['product_price'];
            }

        }
    }

    return $results;
}


/**
 * this function processes and stores the payment info.
 */
function get_payment_info()
{
    if (empty($_POST['payment_method_nonce'])) {
        header('location: pay.php');
    }


    $result = Braintree_Transaction::sale([
        'amount' => $_POST['amount'],
        'paymentMethodNonce' => $_POST['payment_method_nonce'],
        'customer' => [
            'firstName' => $_POST['firstName'],
            'lastName' => $_POST['lastName'],

        ],
        'options' => [
            'submitForSettlement' => true
        ]
    ]);

    if ($result->success === true) {

    } else {
        print_r($result->errors);
        die();
    }
}


